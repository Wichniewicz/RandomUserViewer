package pl.enteralt.infullmobile.randomuserviewer.view.detail.holder;

import android.view.View;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.enteralt.infullmobile.randomuserviewer.R;
import pl.enteralt.infullmobile.randomuserviewer.model.pojo.DetailInfo;
import pl.enteralt.infullmobile.randomuserviewer.view.common.BaseViewHolder;

/**
 * View holder class for {@link DetailInfo}
 */
public class DetailInfoHolder extends BaseViewHolder<DetailInfo, DetailInfoHolder.DetailInfoHolderListener> {

    @Bind(R.id.detailInfoHeader)
    TextView detailHeader;

    @Bind(R.id.detailInfoValue)
    TextView detailValue;

    public DetailInfoHolder(View itemView, DetailInfoHolderListener listener) {
        super(itemView, listener);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setItem(DetailInfo item, int position) {
        super.setItem(item, position);
        detailHeader.setText(item.getHeader());
        detailValue.setText(item.getValue());
    }

    public interface DetailInfoHolderListener {
        // Empty
    }
}
