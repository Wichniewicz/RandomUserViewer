package pl.enteralt.infullmobile.randomuserviewer.presenter.detail;

/**
 * View for {@link pl.enteralt.infullmobile.randomuserviewer.view.detail.activity.DetailActivity}
 */
public interface DetailActivityView {

    /**
     * Shows detailed data
     */
    void onShowData();
}
