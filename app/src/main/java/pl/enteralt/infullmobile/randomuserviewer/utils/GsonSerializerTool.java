package pl.enteralt.infullmobile.randomuserviewer.utils;

import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import pl.enteralt.infullmobile.randomuserviewer.injector.scope.PerApp;
import pl.enteralt.infullmobile.randomuserviewer.model.pojo.Result;

/**
 * GSON serializer tool for serialization of {@link Result} into {@link String} for storing in {@link SharedPreferences}
 */
@PerApp
public class GsonSerializerTool {

    /**
     * Separator for storing in preferences
     */
    private static final String SEPARATOR = "‚‗‚";

    @Inject
    public GsonSerializerTool() {

    }

    /**
     * Serialize random users list
     *
     * @param randomUsersList Random users list
     */
    public String serializeRandomUsersList(List<Result> randomUsersList) {
        Gson gson = new Gson();

        List<String> randomUsersListString = new ArrayList<>();

        for (Result randomUser : randomUsersList) {
            randomUsersListString.add(gson.toJson(randomUser));
        }

        return TextUtils.join(SEPARATOR, randomUsersListString.toArray(new String[randomUsersListString.size()]));
    }

    /**
     * Deserialize random users list
     *
     * @return Deserialized random users list
     */
    public List<Result> deserializeRandomUsersList(String randomUsersFromPreferences) {
        Gson gson = new Gson();

        List<String> randomUsersListString = new ArrayList<>(Arrays.asList(TextUtils.split(randomUsersFromPreferences, SEPARATOR)));
        List<Result> randomUsersList = new ArrayList<>();

        for (String randomUserString : randomUsersListString) {
            randomUsersList.add(gson.fromJson(randomUserString, Result.class));
        }

        return randomUsersList;
    }
}
