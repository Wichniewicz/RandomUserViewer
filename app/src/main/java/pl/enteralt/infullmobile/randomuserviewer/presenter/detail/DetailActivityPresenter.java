package pl.enteralt.infullmobile.randomuserviewer.presenter.detail;

import javax.inject.Inject;

import pl.enteralt.infullmobile.randomuserviewer.injector.scope.PerActivity;
import pl.enteralt.infullmobile.randomuserviewer.presenter.Presenter;

/**
 * Presenter for {@link pl.enteralt.infullmobile.randomuserviewer.view.detail.activity.DetailActivity}
 */
@PerActivity
public class DetailActivityPresenter implements Presenter<DetailActivityView> {

    /**
     * View controlled by presenter
     */
    private DetailActivityView view;

    @Inject
    public DetailActivityPresenter() {
    }

    @Override
    public void bindView(DetailActivityView view) {
        this.view = view;
    }

    /**
     * Shows random user detailed data
     */
    public void showRandomUserData() {
        view.onShowData();
    }
}
