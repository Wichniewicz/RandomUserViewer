package pl.enteralt.infullmobile.randomuserviewer.controller;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.List;

import javax.inject.Inject;

import pl.enteralt.infullmobile.randomuserviewer.app.App;
import pl.enteralt.infullmobile.randomuserviewer.injector.scope.PerApp;
import pl.enteralt.infullmobile.randomuserviewer.model.pojo.Result;
import pl.enteralt.infullmobile.randomuserviewer.utils.GsonSerializerTool;

/**
 * Controller to operate {@link android.content.SharedPreferences}
 */
@PerApp
public class PreferenceController {

    /***
     * Name of application preference file
     */
    private static final String RANDOM_USER_VIEWER_APP_PREFERENCES_NAME = "pl.enteralt.infullmobile.randomuserviewer.preferences";

    /**
     * Key for random user list
     */
    private static final String RANDOM_USERS_LIST_PREF_KEY = "random.users.list.pref";

    /**
     * Shared preferences object to operate on application preferences
     */
    private final SharedPreferences sharedPreferences;

    /**
     * Gson serialization tool object to get / put data from {@link SharedPreferences}
     */
    private GsonSerializerTool gsonSerializerTool;

    @Inject
    public PreferenceController(App context, GsonSerializerTool gsonSerializerTool) {
        this.sharedPreferences = context.getSharedPreferences(RANDOM_USER_VIEWER_APP_PREFERENCES_NAME, Context.MODE_PRIVATE);
        this.gsonSerializerTool = gsonSerializerTool;
    }

    /**
     * Puts list of random users into preferences
     *
     * @param resultList List of random users to put
     */
    public void putRandomUserListInPreferences(List<Result> resultList) {
        sharedPreferences.edit()
                .putString(RANDOM_USERS_LIST_PREF_KEY, gsonSerializerTool.serializeRandomUsersList(resultList))
                .apply();
    }

    /**
     * Get list of random users from preferences
     *
     * @return List of random users
     */
    public List<Result> getRandomUserListFromPreferences() {
        return gsonSerializerTool.deserializeRandomUsersList(sharedPreferences.getString(RANDOM_USERS_LIST_PREF_KEY, ""));
    }
}
