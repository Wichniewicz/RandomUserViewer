package pl.enteralt.infullmobile.randomuserviewer.presenter.main;

import java.util.List;

import pl.enteralt.infullmobile.randomuserviewer.model.pojo.Result;

/**
 * View for {@link pl.enteralt.infullmobile.randomuserviewer.view.main.activity.MainActivity}
 */
public interface MainActivityView {

    /**
     * Received error of fetching random users data
     */
    void onRandomUsersGetError();

    /**
     * Fetched random users stored list
     *
     * @param randomUsersStoredList
     */
    void onRandomUsersStoredListGet(List<Result> randomUsersStoredList);

    /**
     * Random users stored list was not found
     */
    void onRandomUsersStoredListNotFound();
}
