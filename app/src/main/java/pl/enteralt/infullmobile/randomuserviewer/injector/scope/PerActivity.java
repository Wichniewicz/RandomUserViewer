package pl.enteralt.infullmobile.randomuserviewer.injector.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Scope for whole activity lifecycle
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}
