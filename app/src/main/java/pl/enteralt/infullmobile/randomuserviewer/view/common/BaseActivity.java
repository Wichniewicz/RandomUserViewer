package pl.enteralt.infullmobile.randomuserviewer.view.common;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

import pl.enteralt.infullmobile.randomuserviewer.app.App;
import pl.enteralt.infullmobile.randomuserviewer.injector.component.ActivityComponent;
import pl.enteralt.infullmobile.randomuserviewer.injector.component.DaggerActivityComponent;
import pl.enteralt.infullmobile.randomuserviewer.injector.module.ActivityModule;
import timber.log.Timber;

/**
 * Base activity class
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        Timber.d("onCreate");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Timber.d("onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Timber.d("onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Timber.d("onDestroy");
    }

    /**
     * Initializer for {@link ActivityComponent}
     */
    protected static class ActivityComponentInitializer {

        private ActivityComponentInitializer() {
        }

        /**
         * Init injector component.
         *
         * @param activity Activity for adding to dependencies graph.
         *                 Activity needs to extend {@link BaseActivity}
         * @return {@link ActivityComponent}.
         */
        public static <A extends BaseActivity> ActivityComponent init(A activity) {
            return DaggerActivityComponent.builder()
                    .appComponent(App.getAppComponent())
                    .activityModule(new ActivityModule(activity))
                    .build();
        }
    }
}