package pl.enteralt.infullmobile.randomuserviewer.view.main.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pl.enteralt.infullmobile.randomuserviewer.R;
import pl.enteralt.infullmobile.randomuserviewer.model.pojo.Result;
import pl.enteralt.infullmobile.randomuserviewer.view.main.holder.RandomUserHolder;

/**
 * Adapter class for {@link pl.enteralt.infullmobile.randomuserviewer.model.pojo.Result}
 */
public class RandomUserAdapter extends RecyclerView.Adapter<RandomUserHolder> {

    /**
     * List of random users items
     */
    private List<Result> randomUsersList = new ArrayList<>();

    /**
     * Listener for view holder
     */
    private RandomUserHolder.RandomUserHolderListener listener;

    public RandomUserAdapter(@NonNull RandomUserHolder.RandomUserHolderListener listener) {
        this.listener = listener;
    }

    @Override
    public RandomUserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.random_user_holder, parent, false);
        return new RandomUserHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(RandomUserHolder holder, int position) {
        holder.setItem(randomUsersList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return randomUsersList.size();
    }

    /**
     * Add random users to list
     *
     * @param randomUsers List of random users
     */
    public void addRandomUsers(List<Result> randomUsers) {
        randomUsersList.clear();
        randomUsersList.addAll(randomUsers);
        notifyDataSetChanged();
    }
}
