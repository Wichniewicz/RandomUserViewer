package pl.enteralt.infullmobile.randomuserviewer.view.main.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.RelativeLayout;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.enteralt.infullmobile.randomuserviewer.R;
import pl.enteralt.infullmobile.randomuserviewer.model.pojo.Result;
import pl.enteralt.infullmobile.randomuserviewer.presenter.main.MainActivityPresenter;
import pl.enteralt.infullmobile.randomuserviewer.presenter.main.MainActivityView;
import pl.enteralt.infullmobile.randomuserviewer.view.common.BaseActivity;
import pl.enteralt.infullmobile.randomuserviewer.view.detail.activity.DetailActivity;
import pl.enteralt.infullmobile.randomuserviewer.view.main.adapter.RandomUserAdapter;
import pl.enteralt.infullmobile.randomuserviewer.view.main.holder.RandomUserHolder;

public class MainActivity extends BaseActivity implements MainActivityView, RandomUserHolder.RandomUserHolderListener {

    /**
     * Name of parcelable contains random user detailed info
     */
    public static final String RANDOM_USER_PARCELABLE = "random.user.parcelable";

    @Bind(R.id.mainRelativeLayout)
    RelativeLayout mainRelativeLayout;

    @Bind(R.id.mainRandomUsersListRecyclerView)
    RecyclerView randomUsersListRecyclerView;

    @Inject
    MainActivityPresenter presenter;

    /**
     * Adapter to display random users on list
     */
    private RandomUserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initDaggerComponents();
        initViews();
    }

    /**
     * Starts retrieving random users
     */
    private void startRetrievingRandomUsers() {
        Snackbar.make(mainRelativeLayout, R.string.receiving_data, Snackbar.LENGTH_SHORT)
                .show();
        presenter.getRandomUsersFromRest();
    }

    /**
     * Initialize dagger components
     */
    private void initDaggerComponents() {
        ActivityComponentInitializer.init(this).inject(this);
    }

    /**
     * Initialize views
     */
    private void initViews() {
        presenter.bindView(this);
        adapter = new RandomUserAdapter(this);
        randomUsersListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        randomUsersListRecyclerView.setAdapter(adapter);
        presenter.getRandomUsersStoredList();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public void onRandomUsersGetError() {
        Snackbar.make(mainRelativeLayout, R.string.random_users_rest_call_error, Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onRandomUsersStoredListGet(List<Result> randomUsersStoredList) {
        adapter.addRandomUsers(randomUsersStoredList);
    }

    @Override
    public void onRandomUsersStoredListNotFound() {
        startRetrievingRandomUsers();
    }

    @Override
    public void onRandomUserClicked(Result randomUser) {
        Intent detailIntent = new Intent(MainActivity.this, DetailActivity.class);
        detailIntent.putExtra(RANDOM_USER_PARCELABLE, randomUser);
        startActivity(detailIntent);
    }
}
