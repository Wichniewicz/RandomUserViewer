package pl.enteralt.infullmobile.randomuserviewer.view.detail.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pl.enteralt.infullmobile.randomuserviewer.R;
import pl.enteralt.infullmobile.randomuserviewer.model.pojo.DetailInfo;
import pl.enteralt.infullmobile.randomuserviewer.view.detail.holder.DetailInfoHolder;

/**
 * Adapter class for {@link pl.enteralt.infullmobile.randomuserviewer.model.pojo.DetailInfo}
 */
public class DetailInfoAdapter extends RecyclerView.Adapter<DetailInfoHolder> {

    /**
     * List of detail info items
     */
    private List<DetailInfo> detailInfoList = new ArrayList<>();

    @Override
    public DetailInfoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.detail_info_holder, parent, false);
        return new DetailInfoHolder(view, null);
    }

    @Override
    public void onBindViewHolder(DetailInfoHolder holder, int position) {
        holder.setItem(detailInfoList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return detailInfoList.size();
    }

    /**
     * Add detail into to list
     *
     * @param detailInfos List of detail info
     */
    public void addDetailInfo(List<DetailInfo> detailInfos) {
        detailInfoList.clear();
        detailInfoList.addAll(detailInfos);
        notifyDataSetChanged();
    }
}
