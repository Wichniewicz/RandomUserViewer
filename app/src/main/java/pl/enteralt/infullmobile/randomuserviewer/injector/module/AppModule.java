package pl.enteralt.infullmobile.randomuserviewer.injector.module;

import dagger.Module;
import dagger.Provides;
import pl.enteralt.infullmobile.randomuserviewer.app.App;
import pl.enteralt.infullmobile.randomuserviewer.injector.scope.PerApp;

/**
 * Module for Application
 */
@Module
public class AppModule {

    /**
     * Application class
     */
    private final App application;

    public AppModule(App application) {
        this.application = application;
    }

    /**
     * Provides application class object
     *
     * @return Application class
     */
    @PerApp
    @Provides
    public App provideApp() {
        return application;
    }
}