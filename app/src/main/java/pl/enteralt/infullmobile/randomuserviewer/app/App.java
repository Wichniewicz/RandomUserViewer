package pl.enteralt.infullmobile.randomuserviewer.app;

import android.app.Application;

import pl.enteralt.infullmobile.randomuserviewer.BuildConfig;
import pl.enteralt.infullmobile.randomuserviewer.injector.component.AppComponent;
import pl.enteralt.infullmobile.randomuserviewer.injector.component.DaggerAppComponent;
import pl.enteralt.infullmobile.randomuserviewer.injector.module.AppModule;
import pl.enteralt.infullmobile.randomuserviewer.injector.module.NetModule;
import timber.log.Timber;

/**
 * Base application class
 */
public class App extends Application {

    /**
     * Base URL for Rest Api
     */
    private static final String BASE_URL = "http://api.randomuser.me";

    /**
     * Component containing dependencies for whole application lifecycle
     */
    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        initDaggerComponents();
    }

    /**
     * Initialize dagger components for application class
     */
    private void initDaggerComponents() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(BASE_URL))
                .build();
        appComponent.inject(this);
    }

    /**
     * @return {@link AppComponent}
     */
    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
