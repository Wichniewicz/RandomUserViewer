package pl.enteralt.infullmobile.randomuserviewer.injector.component;

import android.support.v7.app.AppCompatActivity;

import dagger.Component;
import pl.enteralt.infullmobile.randomuserviewer.view.detail.activity.DetailActivity;
import pl.enteralt.infullmobile.randomuserviewer.view.main.activity.MainActivity;
import pl.enteralt.infullmobile.randomuserviewer.injector.module.ActivityModule;
import pl.enteralt.infullmobile.randomuserviewer.injector.scope.PerActivity;

/**
 * Component for activities
 */
@PerActivity
@Component(dependencies = {AppComponent.class}, modules = {ActivityModule.class})
public interface ActivityComponent {

    /**
     * Inject dependencies into {@link MainActivity}
     *
     * @param mainActivity {@link MainActivity}
     */
    void inject(MainActivity mainActivity);

    /**
     * Inject dependencies into {@link DetailActivity}
     *
     * @param detailActivity {@link DetailActivity}
     */
    void inject(DetailActivity detailActivity);

    /**
     * @return Activity
     */
    AppCompatActivity activity();
}
