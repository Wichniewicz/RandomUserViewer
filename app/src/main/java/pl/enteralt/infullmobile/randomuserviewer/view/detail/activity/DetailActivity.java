package pl.enteralt.infullmobile.randomuserviewer.view.detail.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.enteralt.infullmobile.randomuserviewer.R;
import pl.enteralt.infullmobile.randomuserviewer.model.pojo.DetailInfo;
import pl.enteralt.infullmobile.randomuserviewer.model.pojo.Result;
import pl.enteralt.infullmobile.randomuserviewer.presenter.detail.DetailActivityPresenter;
import pl.enteralt.infullmobile.randomuserviewer.presenter.detail.DetailActivityView;
import pl.enteralt.infullmobile.randomuserviewer.view.common.BaseActivity;
import pl.enteralt.infullmobile.randomuserviewer.view.detail.adapter.DetailInfoAdapter;
import pl.enteralt.infullmobile.randomuserviewer.view.main.activity.MainActivity;

public class DetailActivity extends BaseActivity implements DetailActivityView {

    /**
     * Email subject
     */
    private static final String EMAIL_SUBJECT = "This is only a Test";

    /**
     * Header title for phones section
     */
    private static final String DETAIL_HEADER_PHONES = "Phones";

    /**
     * Header title for location section
     */
    private static final String DETAIL_HEADER_LOCATION = "Location";

    /**
     * Header title for registration date
     */
    private static final String DETAIL_HEADER_REGISTRATION_DATE = "Registration date";

    @Bind(R.id.detailCoordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    @Bind(R.id.detailToolbar)
    Toolbar toolbar;

    @Bind(R.id.detailUserImage)
    ImageView randomUserImage;

    @Bind(R.id.detailUserName)
    TextView randomUserName;

    @Bind(R.id.detailUserEmail)
    TextView randomUserEmail;

    @Bind(R.id.contentDetailRecyclerView)
    RecyclerView detailInfoRecyclerView;

    @Inject
    DetailActivityPresenter presenter;

    /**
     * Random user clicked on the list
     */
    Result randomUser;

    /**
     * Adapter to display detail info on list
     */
    private DetailInfoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        randomUser = getIntent().getExtras().getParcelable(MainActivity.RANDOM_USER_PARCELABLE);
        ButterKnife.bind(this);
        setupToolbar();
        initDaggerComponents();
        initViews();
    }

    /**
     * Sets toolbar for activity
     */
    private void setupToolbar() {
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    /**
     * Initialize dagger components
     */
    private void initDaggerComponents() {
        ActivityComponentInitializer.init(this).inject(this);
    }

    /**
     * Initialize views
     */
    private void initViews() {
        presenter.bindView(this);
        adapter = new DetailInfoAdapter();
        detailInfoRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        detailInfoRecyclerView.setAdapter(adapter);
        presenter.showRandomUserData();
    }

    /**
     * Populates data in header
     */
    private void populateHeader() {
        Picasso.with(this)
                .load(randomUser.getPicture().getLarge())
                .placeholder(R.drawable.ic_person_black_24dp)
                .fit()
                .centerCrop()
                .into(randomUserImage);
        randomUserName.setText(randomUser.getName().getFullName());
        randomUserEmail.setText(randomUser.getEmail());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.detailFab)
    public void onFabClicked() {
        composeEmail();
    }

    /**
     * Starts email composing
     */
    private void composeEmail() {
        String userEmail = randomUser.getEmail();

        if (userEmail != null) {

            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{userEmail});
            intent.putExtra(Intent.EXTRA_SUBJECT, EMAIL_SUBJECT);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            } else {
                Snackbar.make(coordinatorLayout, R.string.no_default_email_client, Snackbar.LENGTH_SHORT)
                        .show();
            }
        } else {
            Snackbar.make(coordinatorLayout, R.string.email_not_found, Snackbar.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    public void onShowData() {
        populateHeader();
        populateDetailInfoList();
    }

    /**
     * Populates detail info list
     */
    private void populateDetailInfoList() {
        List<DetailInfo> infoList = new ArrayList<>();
        DetailInfo phones = new DetailInfo(DETAIL_HEADER_PHONES, randomUser.getPhones());
        DetailInfo location = new DetailInfo(DETAIL_HEADER_LOCATION, randomUser.getLocation().toString());
        DetailInfo registration = new DetailInfo(DETAIL_HEADER_REGISTRATION_DATE, randomUser.getISORegistrationDate());
        infoList.add(phones);
        infoList.add(location);
        infoList.add(registration);
        adapter.addDetailInfo(infoList);
    }
}
