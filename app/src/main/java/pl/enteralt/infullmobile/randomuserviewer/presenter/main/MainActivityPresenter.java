package pl.enteralt.infullmobile.randomuserviewer.presenter.main;


import java.util.List;

import javax.inject.Inject;

import pl.enteralt.infullmobile.randomuserviewer.controller.PreferenceController;
import pl.enteralt.infullmobile.randomuserviewer.injector.scope.PerActivity;
import pl.enteralt.infullmobile.randomuserviewer.model.pojo.Data;
import pl.enteralt.infullmobile.randomuserviewer.model.pojo.Result;
import pl.enteralt.infullmobile.randomuserviewer.presenter.Presenter;
import pl.enteralt.infullmobile.randomuserviewer.rest.RestApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Presenter for {@link pl.enteralt.infullmobile.randomuserviewer.view.main.activity.MainActivity}
 */
@PerActivity
public class MainActivityPresenter implements Presenter<MainActivityView>, Callback<Data> {

    /**
     * Size of random users list to fetch
     */
    private static final int RANDOM_USERS_LIST_SIZE = 25;

    /**
     * View controlled by presenter
     */
    private MainActivityView view;

    /**
     * REST api to make calls to webservice
     */
    private RestApi service;

    /**
     * Control shared preferences.
     */
    private PreferenceController preferenceController;

    @Inject
    public MainActivityPresenter(RestApi service, PreferenceController preferenceController) {
        this.service = service;
        this.preferenceController = preferenceController;
    }

    @Override
    public void onResponse(Call<Data> call, Response<Data> response) {
        try {
            preferenceController.putRandomUserListInPreferences(response.body().getResults());
            getRandomUsersStoredList();
        } catch (Exception e) {
            e.printStackTrace();
            view.onRandomUsersGetError();
        }
    }

    @Override
    public void onFailure(Call<Data> call, Throwable t) {
        t.printStackTrace();
        view.onRandomUsersGetError();
    }

    @Override
    public void bindView(MainActivityView view) {
        this.view = view;
    }

    /**
     * Fetches random users from REST API
     */
    public void getRandomUsersFromRest() {
        Call<Data> randomUsersData = service.getRandomUsers(RANDOM_USERS_LIST_SIZE);
        randomUsersData.enqueue(this);
    }

    /**
     * Populates random users list from {@link android.content.SharedPreferences} or fetch them from Rest API
     */
    public void getRandomUsersStoredList() {
        List<Result> storedList = preferenceController.getRandomUserListFromPreferences();
        if (storedList.size() > 0) {
            view.onRandomUsersStoredListGet(storedList);
        } else {
            view.onRandomUsersStoredListNotFound();
        }
    }
}
