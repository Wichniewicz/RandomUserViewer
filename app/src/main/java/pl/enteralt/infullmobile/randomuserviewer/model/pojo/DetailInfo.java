package pl.enteralt.infullmobile.randomuserviewer.model.pojo;

/**
 * Detail info class
 */
public class DetailInfo {

    /**
     * Header title
     */
    private String header;

    /**
     * Value text
     */
    private String value;

    public DetailInfo(String header, String value) {
        this.header = header;
        this.value = value;
    }

    /**
     * @return The header
     */
    public String getHeader() {
        return header;
    }

    /**
     * @return The value
     */
    public String getValue() {
        return value;
    }

}
