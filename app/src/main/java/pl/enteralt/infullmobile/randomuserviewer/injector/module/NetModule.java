package pl.enteralt.infullmobile.randomuserviewer.injector.module;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import pl.enteralt.infullmobile.randomuserviewer.injector.scope.PerApp;
import pl.enteralt.infullmobile.randomuserviewer.rest.RestApi;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Module for network components (i.e. Retrofit)
 */
@Module
public class NetModule {

    /**
     * Base URL for retrofit calls
     */
    private String baseUrl;

    public NetModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }


    /**
     * Provides logging interceptor for retrofit
     *
     * @return loggin interceptor object
     */
    @PerApp
    @Provides
    public HttpLoggingInterceptor provideHttpLoggingInterceptor() {

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

    @PerApp
    @Provides
    public OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor) {

        OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(loggingInterceptor).build();
        return okHttpClient;
    }

    /**
     * Provides retrofit class object
     *
     * @return retrofit object
     */
    @PerApp
    @Provides
    public Retrofit provideRetrofit(OkHttpClient okHttpClient) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

    /**
     * Provides REST API class object
     *
     * @param retrofit retrofit object from DI
     * @return REST API object
     */
    @PerApp
    @Provides
    public RestApi provideRestApi(Retrofit retrofit) {

        RestApi service = retrofit.create(RestApi.class);
        return service;
    }
}
