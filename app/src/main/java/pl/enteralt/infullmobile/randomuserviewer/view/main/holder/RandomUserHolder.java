package pl.enteralt.infullmobile.randomuserviewer.view.main.holder;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.enteralt.infullmobile.randomuserviewer.R;
import pl.enteralt.infullmobile.randomuserviewer.model.pojo.Result;
import pl.enteralt.infullmobile.randomuserviewer.view.common.BaseViewHolder;

/**
 * View holder class for {@link Result}
 */
public class RandomUserHolder extends BaseViewHolder<Result, RandomUserHolder.RandomUserHolderListener> {

    @Bind(R.id.randomUserHolderCard)
    CardView randomUserCard;

    @Bind(R.id.randomUserHolderImage)
    ImageView randomUserThumb;

    @Bind(R.id.randomUserHolderName)
    TextView randomUserName;

    @Bind(R.id.randomUserHolderEmail)
    TextView randomUserEmail;

    @Bind(R.id.randomUserHolderDate)
    TextView randomUserDate;

    /**
     * Context for showing image
     */
    private Context context;

    public RandomUserHolder(View itemView, RandomUserHolderListener listener) {
        super(itemView, listener);
        ButterKnife.bind(this, itemView);
        context = randomUserCard.getContext();
    }

    @Override
    public void setItem(Result item, int position) {
        super.setItem(item, position);
        randomUserName.setText(item.getName().getFullName());
        randomUserEmail.setText(item.getEmail());
        randomUserDate.setText(item.getISORegistrationDate());
        Picasso.with(context)
                .load(item.getPicture().getLarge())
                .placeholder(R.drawable.ic_person_black_24dp)
                .fit()
                .centerCrop()
                .into(randomUserThumb);
    }

    @OnClick(R.id.randomUserHolderCard)
    public void onRandomUserClicked() {
        listener.onRandomUserClicked(item);
    }

    /**
     * Interface to communicate with parent component
     */
    public interface RandomUserHolderListener {

        /**
         * Action for click on list element
         *
         * @param randomUser Clicked random user
         */
        void onRandomUserClicked(Result randomUser);
    }
}
