package pl.enteralt.infullmobile.randomuserviewer.injector.component;

import dagger.Component;
import pl.enteralt.infullmobile.randomuserviewer.app.App;
import pl.enteralt.infullmobile.randomuserviewer.controller.PreferenceController;
import pl.enteralt.infullmobile.randomuserviewer.injector.module.AppModule;
import pl.enteralt.infullmobile.randomuserviewer.injector.module.NetModule;
import pl.enteralt.infullmobile.randomuserviewer.injector.scope.PerApp;
import pl.enteralt.infullmobile.randomuserviewer.rest.RestApi;

/**
 * Component for Application
 */
@PerApp
@Component(modules = {AppModule.class, NetModule.class})
public interface AppComponent {

    /**
     * @return Application dependency
     */
    App app();

    /**
     * Inject dependencies into {@link App}
     *
     * @param app {@link App}
     */
    void inject(App app);

    /**
     * @return REST API for accessing to the website
     */
    RestApi restApi();

    /**
     * @return {@link PreferenceController}.
     */
    PreferenceController preferenceController();
}
