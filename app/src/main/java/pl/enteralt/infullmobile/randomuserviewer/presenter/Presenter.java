package pl.enteralt.infullmobile.randomuserviewer.presenter;

/**
 * Interface representing a Presenter in a model view presenter (MVP) pattern.
 *
 * @param <V> View to control by presenter.
 */
public interface Presenter<V> {

    /**
     * Bind view to control by presenter.
     *
     * @param view View to control by presenter.
     */
    void bindView(V view);
}
