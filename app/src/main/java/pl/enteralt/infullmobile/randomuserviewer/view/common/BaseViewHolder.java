package pl.enteralt.infullmobile.randomuserviewer.view.common;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Base view holder class
 */
public class BaseViewHolder<I, L> extends RecyclerView.ViewHolder {

    /**
     * Listener type to communicate with parent component
     */
    protected L listener;

    /**
     * Item type to display in view holder
     */
    protected I item;

    /**
     * Position of Item in adapter
     */
    protected int position;

    public BaseViewHolder(View itemView, L listener) {
        super(itemView);
        this.listener = listener;
    }

    /**
     * Set item to display in view holder
     *
     * @param item     Item to display
     * @param position Position of Item in adapter
     */
    public void setItem(I item, int position) {
        this.item = item;
        this.position = position;
    }
}
