package pl.enteralt.infullmobile.randomuserviewer.rest;

import pl.enteralt.infullmobile.randomuserviewer.model.pojo.Data;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * REST API service
 */
public interface RestApi {

    /**
     * Provides random users
     *
     * @param size number of users to download
     * @return {@link Data}
     */
    @GET("/")
    Call<Data> getRandomUsers(@Query("results") int size);
}
