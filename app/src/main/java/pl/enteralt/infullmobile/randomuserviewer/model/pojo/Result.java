
package pl.enteralt.infullmobile.randomuserviewer.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Result implements Parcelable {

    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("name")
    @Expose
    private Name name;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("login")
    @Expose
    private Login login;
    @SerializedName("registered")
    @Expose
    private Integer registered;
    @SerializedName("dob")
    @Expose
    private Integer dob;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("cell")
    @Expose
    private String cell;
    @SerializedName("id")
    @Expose
    private Id id;
    @SerializedName("picture")
    @Expose
    private Picture picture;
    @SerializedName("nat")
    @Expose
    private String nat;

    /**
     * @return The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return The name
     */
    public Name getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(Name name) {
        this.name = name;
    }

    /**
     * @return The location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location The location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The login
     */
    public Login getLogin() {
        return login;
    }

    /**
     * @param login The login
     */
    public void setLogin(Login login) {
        this.login = login;
    }

    /**
     * @return The registered
     */
    public Integer getRegistered() {
        return registered;
    }

    /**
     * @param registered The registered
     */
    public void setRegistered(Integer registered) {
        this.registered = registered;
    }

    /**
     * @return The dob
     */
    public Integer getDob() {
        return dob;
    }

    /**
     * @param dob The dob
     */
    public void setDob(Integer dob) {
        this.dob = dob;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The cell
     */
    public String getCell() {
        return cell;
    }

    /**
     * @param cell The cell
     */
    public void setCell(String cell) {
        this.cell = cell;
    }

    /**
     * @return The id
     */
    public Id getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Id id) {
        this.id = id;
    }

    /**
     * @return The picture
     */
    public Picture getPicture() {
        return picture;
    }

    /**
     * @param picture The picture
     */
    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    /**
     * @return The nat
     */
    public String getNat() {
        return nat;
    }

    /**
     * @param nat The nat
     */
    public void setNat(String nat) {
        this.nat = nat;
    }

    /**
     * Formats date to fit ISO standard
     *
     * @return Formatted date
     */
    public String getISORegistrationDate() {

        SimpleDateFormat isoDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return isoDateFormat.format(registered);
    }

    /**
     * Get phones concatenation
     *
     * @return Phone numbers
     */
    public String getPhones() {

        StringBuilder phones = new StringBuilder();
        phones.append(phone != null ? phone : "");
        phones.append(cell != null ? "\n" + cell : "");
        return phones.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.name, flags);
        dest.writeParcelable(this.location, flags);
        dest.writeString(this.email);
        dest.writeString(this.phone);
        dest.writeString(this.cell);
        dest.writeInt(this.registered);
        dest.writeParcelable(this.picture, flags);
    }

    public Result() {
    }

    protected Result(Parcel in) {
        this.name = in.readParcelable(Name.class.getClassLoader());
        this.location = in.readParcelable(Location.class.getClassLoader());
        this.email = in.readString();
        this.phone = in.readString();
        this.cell = in.readString();
        this.registered = in.readInt();
        this.picture = in.readParcelable(Picture.class.getClassLoader());
    }

    public static final Parcelable.Creator<Result> CREATOR = new Parcelable.Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel source) {
            return new Result(source);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };
}
